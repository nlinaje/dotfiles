# How to use ansible-vault

Information extracted from ansible documentation: [Link](https://docs.ansible.com/ansible/latest/vault_guide/vault_encrypting_content.html)

## Goal

Ansible vault is used to encrypt secrets and write them in the ansible configuration files.
You can encrypt:

* strings which are saved to variables
* files

## Commands

### Encrypt a string to a variable

The pattern looks like this:

```
ansible-vault encrypt-string <password_source> 'string_to_encrypt' --name '<string_name_of_variable>'
```

Examples:

* The content of the password file "mypassword.txt" contains a password which 
  will be used to encrypt the string "P@ssw0rd". The

```
ansible-vault encrypt_string --vault-password-file 'mypassword.txt' P@ssw0rd --name 'gitlab_password'
```

This commands output the code that must be inserted in a file containing the 
ansible variables. The first word is the variable name which can be referenced
from the ansible playbooks.


```
gitlab_password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          35313463333636663163613139663438356264363737623238383138336533643432303330366636
          6163396561613866656536353565313430306465313162330a356561396366323561663365323664
          34373166303034396466363130316665383134616361376134613363363733373032616433323861
          6436373864383434640a363362656166653434643563616563643366343931376335353939306430
          316
```

* Same example, but this time, we add a label to distinguish between secrets. In this
  example, we change the last command to include "developer" as label. The label is 
  given just before the filename containing the password "<label>@<filename>"

```
ansible-vault encrypt_string --vault-id 'developer@mypassword.txt' P@ssw0rd --name 'gitlab_password'
```

And the output contains the label and the encrypted string.

```
gitlab_password: !vault |
          $ANSIBLE_VAULT;1.2;AES256;developer
          39356139383333643761383439386338336565333665626164316264366563636633306561623532
          3034623061373765646165343861303362376262343361390a316466303434633534363632353966
          61396231626664313534393437626335653431373562346131653138336366366238383537393431
          3634323764383635340a326662303234313331303033646138383861663161643732303831613130
          6462
```

* You can send a string as stdin to the ansible-vault command. You can do it like this:

```
printf "P@ssw0rd" | ansible-vault encrypt_string --vault-id 'developer@mypassword.txt' --stdin-name 'gitlab_password'
```

Note: Use 'printf' instead of 'echo', or you will get a '\n' after decrpytion of your string.
The result is the same as the example before (only the encrpyted data is different each time).


* As this command remains in the history, you can prompt for the string to have more security:


```
ansible-vault encrypt_string --vault-id 'developer@mypassword.txt' --stdin-name 'gitlab_password'
```

It asks you then for the string (it will be displayed), and after pressing <CTRL-D> it will encrypt it:

```
Reading plaintext input from stdin. (ctrl-d to end input, twice if your content does not already have a newline)
P@ssw0rd
Encryption successful
gitlab_password: !vault |
          $ANSIBLE_VAULT;1.2;AES256;developer
          34356262383335316339313937313965346536643430363830623863356334356537373662336331
          6533323833646134376362626630353636633230613865380a636436656334336164616637323837
          37646534653834366365373665376534373738633638396562646439396534303165323532313264
          3938633930303733620a373765643365376639666130663837306261636234366363383433386134
          3064
```

### Decrypt the string from a variable for debug

If you have put one of the examples above in the file 'vars.yml', then you can decrypt it 
with the following command. 

```
ansible localhost -m ansible.builtin.debug -a var='gitlab_password' -e "@vars.yml" --vault-id developer@mypassword.txt
```

And the output of the command contains the variable and string that you have encrypted:

```
localhost | SUCCESS => {
    "gitlab_password": "P@ssw0rd"
}
```

Be sure that when decrypting, the file "mypassword.txt" contains the password string used to encrypt the string.

### Encrypt files

Following examples will be done under `/tmp/vault-examples`:

* Create a file with secret contents and encrypt the file. Ask the user for password
  to encrypt in the prompt and put the label "developer".

```
echo "I dont want anybody to see this" > file_with_secrets.txt
ansible-vault encrypt --vault-id developer@prompt file_with_secrets.txt
```

After executing the command, it asks for a password twice (it is not showned) and then
it encrypts the file. This is the contents of the file after encryption:

```
$ cat file_with_secrets.txt 
$ANSIBLE_VAULT;1.2;AES256;developer
31383037663732633962633937663364646661646437373664633365626635326637303861333538
3033326161376433646261653261333862656263343961330a333539393139333966323738636630
32346132363566393036386662326534373732333864373736613264613534386362303834613966
3033393030336537610a303163386264343763353438303865346262663231313363663562636662
32646238373831393335633432316131646534663966633230346237336338666637623931363362
3934333561303066393336623434303166653663313334313438
```

* Edit the encrypted file in place

In this case, after entering the password, the editor from environment variables
$EDITOR is opened, and after changing and saving the file, the new contents
are encrypted again.

```
ansible-vault edit --vault-id developer@prompt file_with_secrets.txt 
```

* Use a password file to encrypt a file 

Instead of entering the password in the prompt, you can have the password in a 
file. This file can be used to encrypt/edit/decrypt the secret file. In this
case, our password is saved in the file "mypasswordfile.txt". The first command
shows how to encrypt a file, and the second how to edit it.

```
ansible-vault encrypt --vault-id developer@mypasswordfile.txt file_with_secrets.txt
ansible-vault edit --vault-id developer@mypasswordfile.txt file_with_secrets.txt
```

* Change the key

You may want to change the "password" that has been used to encrypt a file. The 
following example, prompts for the old password and then for the new password twice.

```
ansible-vault rekey --vault-id developer@prompt file_with_secrets.txt
```

The process is like this when the vault password and the confirm password
matches the new password:

```
Vault password(developer):
New Vault password:
Confirm New Vault password:
Rekey successful.
```

Now, to decrypt/edit the file, you must use the new password.

### Decrypt a file

* To decrypt a file, you can prompt for the password:

```
ansible-vault decrypt file_with_secrets.txt
```

After entering the password, the "ifle_with_secrets.txt" has plain text and
everybody can read it again.

* You can decrypt too, if you have your password (in plain text) saved in a file.

```
ansible-vault decrypt --vault-id developer@password_file.txt file_with_secrets.txt
```

## Secrets in this project

The dotfiles script, uses the variable $VAULT_SECRET to know where is the password saved.
In order that the playbook works, you must be sure that the file set by this variable
contains the correct password.

In this project we protect following secrets: (list may not be updated, check the variables file)

* the gitlab email address in variable gitlab_user_email

To encrypt it, just use the following command:

```
echo "mypassword" > mypassword.txt
printf "address@email.com" | ansible-vault encrypt_string --vault-id 'gitlab@mypassword.txt' --stdin-name 'gitlab_user_email'
```


