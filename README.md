# dotfiles

Provide fully automated bootstrap after a linux installation including all dotfiles.


## Goals

Easy way to automate the post installations after a fresh Fedora installation using Ansible.

Additionally, add all dotfiles.

In the future, it may provide post installations for other distributions.

## Why Ansible

After testing many other ways to manage the dotfiles like yadm or stow, I want to give
Ansible a try.

## Author

Ignacio Linaje

## Acknowledgment

The idea of this project has been taken from many different sources:

For Ansible management part:

- [TechDufus] (https://github.com/TechDufus/dotfiles)
- [LearnLinuxTV] (https://www.youtube.com/watch?v=sn1HQq_GFNE)

Beautiful Output callback in ansible:
- [Thiago Alves / Townk] (https://github.com/Townk/ansible-beautiful-output)

Dotfiles:

- [] ()
