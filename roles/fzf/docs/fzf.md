# FZF - How to use it - Summary

More information can be found under its [Github page](https://github.com/junegunn/fzf)
or `man fzf`.

## Installation

* With package manager (yum/apt/...) - may not be the last version
* Homebrew (`brew install fzf`)
* Using git (`git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf; ~/.fzf/install`)

## Upgrade

* With package manager (yum/apt/...) - may not be the last version
* Homebrew (`brew update; brew upgrade fzf`)
* Using git (`cd ~/.fzf; git pull && install`)

## Usage

fzf is a fuzzy finder. Per default, it displays a list of files, and after the 
user selects one, it sends it to stdout. The layout may vary depending on some 
configuration variables. The following is a possible layout:

```
> <prompt> < <stats>                
file1                  Use <CTRL-K> to move up in the list
file2                  Use <CTRL-J> to move down in the list
....                   Press Enter to select item in the list
....                   On multi-select (use -m), press TAB, Shift-Tab to select multiple items
filen                  Press ESC or CTRL-C/CTRL-G to cancel
```

In the prompt, the user writes the filename it is searching. The stats displays
the number of files being displayed and the total number of files found.

There are many ways to use it. Here some examples:

* Give a list of all files and fuzzy find one file. Selected one will be output

```
> find * -type f | fzf
> fd | fzf
> fzf
```

* Select a file and edit it with nvim:

```
> nvim $(fzf)
> fzf -print0 | xargs -0 -o nvim
> fzf
```

In fact, fzf can be used for more than files. It really gets a list of items
from stding and then the selected item is sent to stdout. Per default, the stdin 
is the `find` command, that's why per default it searches for files. But you
can put in stdin anything else. Examples:

```
> git branch | fzf
> cat list_of_items | fzf
```

### Search syntax

in the prompt you can use following tokens to filter the list:

* sbrtk   - Items that match sbrtk
* 'wild   - Items that include 'wild'
* ^music  - Items that start with 'music'
* .mp3$   - Items that end with '.mp3'
* !fire   - Items that do not contain 'fire'
* !^music - Items that do not start with 'music'
* !.mp3$  - Items that do not end with '.mp3'
* ^core 'gdb - Items that start with core AND contains 'gdb'
* ^core py$ | go$ - Items that start with core AND end with 'py' OR 'go'

### Keybindings

Depending on the installation type, the keybindings may be enabled (using install from git)
or disabled (i.e. in Fedora)

In Fedora, you need to source the files in `/usr/share/fzf/shell`, i.e. `/usr/share/fzf/shell/key-bindings.bash`

When the key-binding script is enabled you will have:

* CTRL-T: Trigger fzf 
  Example: `> nvim <CTRL-T>`: passes the selected items in fzf to nvim
* CTRL-R: Fuzzy find from your history
* ALT-C: Change current working directory.
  Example: `> <ALT-C>`: after selecting the directory, it goes there

## Fuzzy completion for bash

In order that this work, you must be sure that the file `/etc/bash_completion.d/fzf` has
been sourced from a dotfile.

It will be triggered if you press `tab` after the fzf trigger sequence (default `**`)

It allows to use autocompletion with fzf for the following:

* Files and directories (`cd /home/**<tab>`, `cat /etc/s**`)
* Aliases (`unalias **<tab>`)
* Environment variables (`unset **<tab>`, `export **<tab>`)
* Hostnames (`ssh **<tab>`)
* Process IDs (`kill -9 **<tab>` => fzf from `ps` command)

Additionally, the `fzf` completion script, contains the function `__fzf_comprun` which
calls an undefined function named `_fzf_comprun`. You can tweak the behaviour of fzf 
by declaring this function in one of your dotfiles. Example of this function in the
documentation:

```
# Advanced customization of fzf options via _fzf_comprun function
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf --preview 'tree -C {} | head -200'   "$@" ;;
    export|unset) fzf --preview "eval 'echo \$'{}"         "$@" ;;
    ssh)          fzf --preview 'dig {}'                   "$@" ;;
    *)            fzf --preview 'bat -n --color=always {}' "$@" ;;
  esac
}
```

## Environment variables

* FZF_DEFAULT_COMMAND: Contains the default command to use as stdin for the tool
* FZF_DEFAULT_OPTS: Contains options like layout, inline info to modify how it looks like
* FZF_CTRL_T_COMMAND: Override the CTRL-T Keybinding command
* FZF_CTRL_T_OPTS: Options for the CTRL-T Keybinding
* FZF_CTRL_R_OPTS: Options for the CTRL-R Keybinding
* FZF_ALT_C_COMMAND: Override the ALT-C Keybinding command
* FZF_ALT_C_OPTS: Options for the ALT-C Keybinding
* FZF_COMPLETION_TRIGGER: Trigger sequence for autocompletion (default: `**`)
* FZF_COMPLETION_OPTS: Options for the autocompletion fzf command


