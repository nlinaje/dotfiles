#!/usr/bin/env bash

# Load keybindings for bash 
KEYBINDINGS_FILE="/usr/share/fzf/shell/key-bindings.bash"

# shellcheck source=/usr/share/fzf/shell/key-bindings.bash
[[ -n "$KEYBINDINGS_FILE" ]] && source "$KEYBINDINGS_FILE"
unset KEYBINDINGS_FILE

export FZF_DEFAULT_OPTS='--height 40% --layout=reverse --border --inline-info'
export FZF_DEFAULT_COMMAND='fd --type f'

# This function is called internally by `__fzf_comprun` in file 
# `/etc/bash_completion.d/fzf`
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf --preview 'tree -C {} | head -200'   "$@" ;;
    export|unset) fzf --preview "eval 'echo \$'{}"         "$@" ;;
    ssh)          fzf --preview 'dig {}'                   "$@" ;;
    *)            fzf --preview 'bat -n --color=always {}' "$@" ;;
  esac
}

#: FZF_DEFAULT_OPTS
#:: --height (%): Vertical part of the terminal to show files
#:: --layout: Use --reverse to put the prompt at the top instead of the bottom
#:: --border: Draw a border line
#:: --inline-info: Show statistics (num files being shown/numfiles available) in 
#::                the same line as the prompt
#:: --border: Apply a padding to the prompt and files to separate it from other data
