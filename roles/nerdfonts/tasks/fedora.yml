---
- name: Nerd Fonts | ensure fonts directory exists
  file:
    path: "{{ ansible_user_dir }}/.local/share/fonts"
    state: directory

- name: Nerd Fonts | Get current version of nerd fonts in Github
  ansible.builtin.shell: >
      curl --silent https://api.github.com/repos/{{ gh_user }}/{{ gh_repo }}/releases/latest |
      grep '"tag_name":' |
      sed -E 's/.*"([^"]+)".*/\1/' |
      sed 's/v//'
  register: command_result
  changed_when: false
  failed_when: >
    command_result.rc != 0 or
    not command_result.stdout|regex_search('\w+\.\w+\.\w+')
  when: target_nerdfonts_version|length == 0

- name: Nerd Fonts | Set latest version of nerd fonts
  ansible.builtin.set_fact:
    target_nerdfonts_version: "{{ command_result.stdout }}"
  when: target_nerdfonts_version|length == 0

- name: Nerd Fonts | Hack exists
  shell: "ls {{ ansible_user_dir }}/.local/share/fonts/Hack*Nerd*Font*"
  register: hack_exists
  ignore_errors: yes

- name: Nerd Fonts | Download Hack
  when: hack_exists is failed
  ansible.builtin.unarchive:
    src: "https://github.com/ryanoasis/nerd-fonts/releases/download/v{{ target_nerdfonts_version }}/Hack.zip"
    dest: "{{ ansible_user_dir }}/.local/share/fonts/"
    remote_src: yes

- name: Nerd Fonts | DejaVu exists
  shell: "ls {{ ansible_user_dir }}/.local/share/fonts/DejaVuSans*Font*"
  register: dejavu_exists
  ignore_errors: yes

- name: Nerd Fonts | Download DejaVu
  when: dejavu_exists is failed
  ansible.builtin.unarchive:
    src: "https://github.com/ryanoasis/nerd-fonts/releases/download/v{{ target_nerdfonts_version }}/DejaVuSansMono.zip"
    dest: "{{ ansible_user_dir }}/.local/share/fonts/"
    remote_src: yes

- name: Nerd Fonts | Fantasque exists
  shell: "ls {{ ansible_user_dir }}/.local/share/fonts/FantasqueSans*Font*"
  register: fantasque_exists
  ignore_errors: yes

- name: Nerd Fonts | Download Fantasque
  when: fantasque_exists is failed
  ansible.builtin.unarchive:
    src: "https://github.com/ryanoasis/nerd-fonts/releases/download/v{{ target_nerdfonts_version }}/FantasqueSansMono.zip"
    dest: "{{ ansible_user_dir }}/.local/share/fonts/"
    remote_src: yes

- name: Nerd Fonts | UbuntuMono exists
  shell: "ls {{ ansible_user_dir }}/.local/share/fonts/UbuntuMono*Font*"
  register: ubuntumono_exists
  ignore_errors: yes

- name: Nerd Fonts | Download UbuntuMono Font
  when: ubuntumono_exists is failed
  ansible.builtin.unarchive:
    src: "https://github.com/ryanoasis/nerd-fonts/releases/download/v{{ target_nerdfonts_version }}/UbuntuMono.zip"
    dest: "{{ ansible_user_dir }}/.local/share/fonts/"
    remote_src: yes

- name: Nerd Fonts | Source Code Pro exists
  shell: "ls {{ ansible_user_dir }}/.local/share/fonts/SauceCodeProNerd*Font*"
  register: sourcecodepro_exists
  ignore_errors: yes

- name: Nerd Fonts | Download Source Code Pro Font
  when: sourcecodepro_exists is failed
  ansible.builtin.unarchive:
    src: "https://github.com/ryanoasis/nerd-fonts/releases/download/v{{ target_nerdfonts_version }}/SourceCodePro.zip"
    dest: "{{ ansible_user_dir }}/.local/share/fonts/"
    remote_src: yes

- name: Nerd Fonts | LiberationMono Font exists
  shell: "ls {{ ansible_user_dir }}/.local/share/fonts/Literation*Font*"
  register: liberation_exists
  ignore_errors: yes

- name: Nerd Fonts | Download Liberation Mono Font
  when: liberation_exists is failed
  ansible.builtin.unarchive:
    src: "https://github.com/ryanoasis/nerd-fonts/releases/download/v{{ target_nerdfonts_version }}/LiberationMono.zip"
    dest: "{{ ansible_user_dir }}/.local/share/fonts/"
    remote_src: yes
