# Gnome Extensions

## Goal

Gnome extensions are additional programs that can extend the functionality of the gnome-shell.
Examples: display temperature/usage/... in the panel, add drive mount/unmount button in the panel
connect to Android phones...

## Discover

In this [link](https://extensions.gnome.org/) you can discover the available
extensions for Gnome Shell. Extensions are usually not compatible between
gnome-shell versions. The developer must update his extension for the new gnome
version.

## Installation

### Automatic installation

To install a gnome extension, you can just press the "Install" Button in Firefox,
which does enables it automatically.

### Manual installation

1. Download the extension
2. Extract it under ~/.local/share/gnome-shell/extensions/
3. Restart gnome-shell
  3.1. In X11, just press Alt-F2, "r"<Enter>
  3.2. In wayland, logout and login again (although Firefox can enable it automatically
  it seems there is no way to enable it from the terminal!)
4. Enable the extension
```
> gnome-extension list --user
> gnome-extension enable <extension-uuid>
```

## Tools

There are two
