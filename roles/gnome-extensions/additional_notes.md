# Link

Ansible - install gnome-extensions copied from here:

https://github.com/jaredhocutt/ansible-gnome-extensions

# Changes

The original ansible code installs everything manually, but at least in 
Wayland does not work properly, because before enabling the extension 
the user has to logout and login again. Then, the extension can be enabled.
This is a problem, because the extensions will not be enabled until 
the user runs the playbook again after loging in again.

Workaround found:

Use busctl command to install the extension the same way as some browsers 
do. The command is:

```
busctl --user call org.gnome.Shell /org/gnome/Shell \
       org.gnome.Shell.Extensions InstallRemoteExtension \
       s {{ gnome_extension_info.uuid}}
```

The problem with this workaround is, that for each extension to be installed,
a pop up appears and asks the user if he wants to install the extension or not.
This is not good for automatization, but it's better that not having the 
extensions enabled at all.
