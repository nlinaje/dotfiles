# Zoxide - How to use it - Summary

More information can be found in the  [Github Page](https://github.com/ajeetdsouza/zoxide)

zoxide is a smarter cd command, inspired by z and autojump.

It remembers which directories you use most frequently, so you can "jump" to 
them in just a few keystrokes. As it is saved in a database, it remembers
the directories for all terminal windows in a single place, even after reboot.

## Installation

* Via install script (recommended): `curl -sS https://raw.githubusercontent.com/ajeetdsouza/zoxide/main/install.sh | bash`
* With package manager (yum/apt/...) - may not be the last version
* Homebrew (`brew install zoxide`)

## Setup in bash

Add following line to your bash dotfiles:

`eval "$(zoxide init bash)"`

Zoxide uses `fzf`, so you need to install it too.

## Usage

```
> z /some/really/long/path    => cd to /some/really/long/path
> z                           => cd to $HOME
> z path                      => cd to /some/really/long/path
> z /another/really/long/path => cd to /another/really/long/path
> z                           => cd to $HOME
> z path<space><tab>          => show interactive completion
> zi <enter>                  => start fzf to jump to directory with zoxide
> zi path<enter>              => start fzf to jump to directory with zoxide (put path in fzf prompt)
> z -                         => cd to previous directory
```

## Configuration

### Flags

* `--cmd`: Changes the prefix of `z` and `zi`. Example: `--cmd j` changes commands to `j`
  and `ji` instead of `z` and `zi`, or `--cmd cd` to `cd` and `cdi` (not in POSIX shells)
* `--hook <HOOK>`: Changes zoxide score increment behaviour:
  HOOK: `never`, `prompt` (at every new shell prompt) and `pwd` (only when directory changes)

### Environment variables

* `_Z_DATA_DIR`: Directory where database is stored (default: `$HOME/.local/share/zoxide`)
* `_Z_ECHO`: If set to 1, prints the directory before jumping there
* `_Z_EXCLUDE_DIRS`: Exclude dirs from database (uses ':' as separator). Example: "$HOME:$HOME/.local/share"
* `_Z_FZF_OPTS`: Custom options for `fzf`
* `_Z_MAXAGE`: Maximum number of entries in database (default: 10000)
* `_Z_RESOLVE_SYMLINKS`: When set to 1, resolves symlinks before inserting it in the database.
