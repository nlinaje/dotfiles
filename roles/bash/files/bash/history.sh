#!/usr/bin/env bash
#
_history_strategy="combined" # linux_default, combined or multiplexed
_history_dir="$HOME/.cache/bash/history"

[[ -d "$_history_dir" ]] || mkdir -p "$_history_dir"
[[ -d "$_history_dir" ]] && chmod 0700 "$_history_dir"

_history_strategy_linux_default() {
  HISTFILE="$_history_dir/history_file"
}

_history_strategy_combined() {
  HISTFILE="$_history_dir/history_file"
  PROMPT_COMMAND="_history_sync; $PROMPT_COMMAND"
}

_history_strategy_multiplexed() {
  HISTFILE="$_history_dir/history.$(date +%y%b%d-%H%M%S).$$"
  PROMPT_COMMAND="_history_sync; $PROMPT_COMMAND"
}

_history_sync() {
  builtin history -a # Append to history file
}

case "$_history_strategy" in
  linux_default) _history_strategy_linux_default;;
       combined) _history_strategy_combined;;
    multiplexed) _history_strategy_multiplexed;;
              *) echo "Error: unknown history strategy in $0"
esac

export HISTSIZE=-1
export HISTFILESIZE=99999
export HISTTIMEFORMAT="[%d/%m/%Y %T] "

export HISTCONTROL=ignorespace
export HISTIGNORE=""

unset _history_dir _history_strategy _history_strategy_linux_default
unset _history_strategy_combined _history_strategy_multiplexed

shopt -s histappend
shopt -s histreedit
shopt -s histverify
shopt -s cmdhist
shopt -s lithist

#: History options in bash:
#: --- History environment variables ---
#:: HISTFILE:       Path to history file
#::
#:: HISTFILESIZE:   Maximum number of lines in the history file. When the
#::                 history file exceeds this size, the oldest entries are
#::                 removed.
#::
#:: HISTSIZE:       Number of commands in history list (commands available to
#::                 user when using the up/down arrows) of current session
#::                 HISTSIZE=0 (disable history list)
#::                 HISTSIZE=-1 (history list without limit)
#::
#:: HISTCONTROL:    manages what history is stored
#::                 ignorespace: Ignore commands starting with empty space
#::                 ignoredups: Ignore duplicate commands
#::                 ignoreboth: Ignorespace + ignoredups
#::                 erasedups: Remove duplicated commands from history
#::
#:: HISTTIMEFORMAT: Specifies the format of timestamps associated with history
#::                 entries. When set, it includes the timestamp along with the
#::                 command in the history file.
#::
#:: HISTIGNORE:     A colon-separated list of patterns that specifies commands 
#::                 to be ignored by the history expansion feature. 
#::                 HISTIGNORE=”ls:ll:exit:clear:cd:top:htop*:history*:rm*”
#::
#:: HISTCMD:        Contains the current history command number.
#::              
#::              
#: --- History behaviour modification with shopt command ---
#:: `shopt -s <option>` to enable, `shopt -u <option` to disable
#:: histappend : when writing history list to history file when session ends,
#::              it appends the history instead of overwriting it. 
#:: histreedit : if enabled, any expression not found in the history list 
#::              (i.e. "!echoo") will display an error "event not found", but
#::              the prompt will allow you to edit the expression again
#:: histverify : if enabled, the expression "!123" does not execute immediately
#::              the command in history position 123. User is able to edit it
#::              first. Pressing Enter again, executes the command.
#:: cmdhist    : if enabled (default), multiline commands are saved as one 
#::              entry in the history list. If disabled, each line is saved as
#::              one entry. See lithist for an example, as the history saves
#::              it differently depending on lithist status.
#:: lithist    : if enabled (and cmdhist is enabled), multiline commands are
#::              saved with embedde newlines.
#::              Example:
#::              ```
#::              for x in one two; do
#::              > echo "Test: $x"
#::              > done
#::              ```
#::              History depending on cmdhist/lithist state (-: not set, +: set)
#::              -cmdhist:
#::              1 for x in one two; do
#::              2 echo "Test: $x"
#::              3 done
#::
#::              +cmdhist-lithist             
#::              1 for x in one two; do echo "Test: $x"; done
#::
#::              +cmdhist+lithist
#::              1 for x in one two; do
#::              echo "Test: $x"
#::              done
#::              
#::              
#: --- History shell options ---
#:: Options are read with `set -o`, enabled with `set +o <option>` and disabled
#:: with `set -o <option>`
#:: Options:
#::   history:    Enables history command (default in interactive shells)
#::   histexpand: Allows to use history substitution (see event designators, 
#::               word designators and modifiers) => ("!echo")
#::
#::
#: --- Terminal shortcuts (emacs mode) ---
#:: CTRL-p  : Fetch previous command from history
#:: CTRL-n  : Fetch next command from history
#:: CTRL-r  : Search history backward (incremental search)
#:: CTRL-s  : Search history forward (incremental search)
#::           Only if XON/XOFF flow control (software flow control) is disabled
#::           (`stty -ixon`) or set to another key (`stty stop '^T'`) - remember
#::           that after stoping the flow control you can start it again with
#::           CTRL-q
#:: PageUp  : Search backward using non-incremental search. Advice:
#::           Put "\ep: history-search-backward" in your inputrc for "ALT-p"
#:: PageDown: Search forward using non-incremental search. Advice:
#::           Put "\en: history-search-forward" in your inputrc for "ALT-n"
#:: Alt-<   : Move to first line in the history
#:: Alt->   : Move to the end of the history
#::              
#::              
#: --- History commands ---
#:: history --help : displays help of history command
#:: history        : displays history commands from contents of the history
#::                  file contents at the time the session was opened plus
#::                  the history list since session start
#::                  if history is deleted in one session, another opened
#::                  session still remembers the deleted history 
#:: history n      : display last 'n' commands from history file+list
#:: history -a     : appends new history list entries to history file 
#::                  (it does not overwrite the file)
#:: history -c     : Clears entire command history list (not from file)
#:: history -d n   : Deletes history entry number 'n' from history
#::                  list. To delete it from history file you must write
#::                  it again with `history -w` but this will delete all
#::                  history commands saved from previous sessions
#:: history -n     : Synchronize history file with current session history list
#::                  For example:
#::                   - Session 1: Appends current list to file (`history -a`)
#::                   - Session 2: Reads appended entries from session 1 into
#::                     its own session history list (`history -n`)
#:: history -r     : read current history list and append it to the 
#::                  history list (again) - not in history file
#::                  history list has all commands repeated
#:: history -s arg : Add 'arg' to history list (not file) without executing it
#::                  Note: almost the same can be achieved by leading char '#' 
#::                  in your command ('echo foo' => '#echo foo')
#:: history -p arg : Echoes 'arg' without adding anything to history list
#:: history -P arg : Echoes 'arg' without adding anything to history list
#:: history -w     : Write current history list to history file (overwrite 
#::                  old history)
#::              
#::              
#: --- History expansion --- (see `man history` for more info)
#:: Event designators
#::    !                : Start history substitution
#::    !!               : Last commands
#::    !#               : Entire command line typed so far
#::    !n               : n-th command line (from the beginning of history file)
#::    !-n              : Current command line minus n
#::    !string          : Most recent command starting with 'string'
#::    !?string?        : Most recent command containing 'string' (last ? optional)
#::    ^string1^string2 : Quick substitution (string1 replaced by string2 in last command)
#:: Word designators (follow event designators, separated by colon)
#::    0   : first word in a line (usually command name)
#::    n   : n-th word in a line
#::    ^   : first argument (second word in a line)
#::    $   : last argument in a line
#::    %   : word matched by most recent ?string? search
#::    x-y : range of words from x to y (-y synonymos of 0-y)
#::    *   : all words but the zeroth
#::    x*  : synonymous with x-$
#::    x-  : words from x to the second to last word
#:: Modifiers (follow word designators, separated by colon)
#::    h          : Remove trailing pathname, leave the head
#::    t          : Remove all leading pathname, leave the tail
#::    r          : Remove trailing suffix ".xxx" leave basename
#::    e          : Remove all but the trailing suffix
#::    p          : Print resulting command but do not execute it
#::    q          : Quote substituted words, excaping further substitutions
#::    x          : Quote substituted words, breaking them into words at blanks and newlines
#::    s/old/new/ : Substitute new for old
#::    &          : Repeat previous substitution
#::    g          : Causes s/old/new/ or & to be applied over the entire event line
#::    G          : Apply 's' or '&' modifier once to each word in the event line
#::
#:: Examples:
#::    $ echo a b c d e
#::    a b c d e
#::    $ echo !!:3-$
#::    c d e
#::    $ echo !-2:*:q
#::    a b c d e
#::    $ echo !-3:1:2:4:x
#::    a b d
#::    $ tar xvf package-x.y.z.tgz
#::    $ cd !-1:$:r => executes `cd package-x.y.z)
#::
#::
#: --- History strategy ---
#:: This script allows to decide between 3 strategies to use for the history
#:: file:
#:: - linux_default: history is saved to history file when the user logs out
#:: - combined:      history is saved after each command preserving the history
#::                  of each terminal window in a single file
#:: - multiplexed:   history is saved after each command preserving the history
#::                  of each terminal window in multiple files - one file for
#::                  each terminal window opened
#:: The combined and multiplexed strategies copy all history commands 
#:: immediately in the history file, but the history list is not available
#:: in running terminals. In order that running terminals append commands
#:: from other terminals, the commands "history -c; history -r" could be used
#:: but this behaviour seems a little odd. More info in 
#:: https://unix.stackexchange.com/questions/1288/preserve-bash-history-in-multiple-terminal-windows
