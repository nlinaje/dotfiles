#!/usr/bin/env bash

alias alert-low='_alert "low"'
alias alert-normal='_alert normal'
alias alert-critical='_alert critical'

_alert() {
  declare notification_level=$1
  notify-send --urgency="$notification_level" \
              -i "$([ $? = 0 ] && echo terminal || echo error)" \
              "$(history | tail -n 1 | \
                 sed 's/^\s*[0-9]\+\s*//' | \
                 sed 's/^\[.*\] //' | \
                 sed 's/[;&|]\s*alert-[a-z]*\s*$//')"
}

#: Send desktop notifications when a command finish its execution
#:: Use notify-send command - the desktop manager will receive it and display 
#:: a notification. It works with Gnome, KDE, Xfce...
#:: In order that it works in tiling window managers, you need additionally
#:: a notification daemon or service running in the backgroud to display the
#:: notifications (i.e. "dunst")

#:: How to use it - Just start a command and append and use ";" or "|" or "&"
#:: afterwards and execute one alert alias. Examples:
#::   sleep 1; alert-low
#::   gunzip bigfile.gz | alert-normal
#::   find / -print > /tmp/list & alert-critical

#:: How does the command work (i.e. command: `sleep 1; alert-normal`:
#::  notify-send --urgency="$notification_level" \ => send notification to desktop
#::      -i "$([ $? = 0 ] && echo terminal || echo error)" \ => If command success => Use terminal icon in notification
#::                                                             else               => Use error icon in notification
#::	      "$(history | tail -n 1 | \                     => Get the last command executed from history (1323 sleep 1; alert-normal)
#::	         sed 's/^\s*[0-9]\+\s*//' | \                => Remove the history number column at the beginning (sleep 1; alert-normal)
#::	         sed 's/[;&|]\s*alert-[a-z]*\s*$//')"        => Remove the "alert" string command (sleep 1)
