# RIPGREP - How to use it - Summary

ripgrep is a line-oriented search tool that recursively searches the current
directory for a regex pattern. It respects gitignore rules and automatically
skip hidden files/directories and binary files. 

More information can be found under its [Github page](https://github.com/BurntSushi/ripgrep),
`man rg` or `rg --help`.

## Installation

* With package manager (yum/apt/...) - may not be the last version (`sudo dnf ripgrep`)
* Homebrew (`brew install fzf`)
* Get binary from [github](https://github.com/BurntSushi/ripgrep/releases)

## Usage

ripgrep (`rg`) is an alternative to `grep` or other tools like `ag` (The Silver Searcher) 
or `ack`.

The basic usage of the tool is `rg <word> <file>`. It looks for the 
word '<word>' in the file '<file>'. If it finds that word in a line of 
that file, then it is sent to stdout. Here some examples on how to use 
it which show some regular expressions. More regular expressions can be 
found [here](https://docs.rs/regex/*/regex/#syntax):

```
> rg fast readme.md            => Searches for word "fast" in "readme.md" file
> rg 'fast\w+' readme.md       => Word "fast" followed by one  or more characters
> rg 'fast\w*' readme.md       => Word "fast" followed by zero or more characters
> rg 'bash'                    => Word "bash" in all files in current directory (and recursive directories)
> rg 'bash' src                => Word "bash" in all files in src directory (and recursive directories)
> rg --maxdepth 1 'bash' src   => Word "bash" in all files in src directory (not recursive directories)
> rg 'function write\('        => Function write in all files (recursively) - Escape "(" with "\"
                                  because it has special significance in regex expressions
> rg test -g '*.py'            => Search "test" ONLY in python files
> rg test -g '!*.py'           => Search "test" NOT in python files
> rg test -g '!*.py' -g '*.py' => Search "test" ONLY in python files (second "-g" overrides first one)
> rg test --type rust          => Search "test" ONLY in rust files
> rg test -trust               => Search "test" ONLY in rust files
> rg test -tpy                 => Search "test" ONLY in python files
> rg test -tpython             => Search "test" ONLY in python files
> rg --type-list               => Show all rigpreg known type files
> rg --type-list | rg '^py'    => Show ripgrep known type files which start with "py"
> rg test --type-not rust      => Search "test" NOT in rust files
> rg test --Trust              => Search "test" NOT in rust files
> rg test --Trust              => Search "test" NOT in rust files
> rg --type-add 'web:*.html' --type-add 'web:*.css' --type-add 'web:*.js' -tweb title
                               => Add type web for "html/css/js" files and give them the type name "web"
> rg --type-add 'web:*.{html,css,js}' -tweb title
                               => Add type web for "html/css/js" files and give them the type name "web"
                               => You can add this in your configuration file
> rg test -tall                => Search "test" only in the known type files

```

### Replacements

Ripgrep can replace words (similar to `sed`). Example:

```
> rg fast readme.md                 => Searches for word "fast" in "readme.md" file
> rg fast readme.md --replace FAST  => Replaces word "fast" with "FAST" in "readme.md" file
> rg fast readme.md -r FAST         => Replaces word "fast" with "FAST" in "readme.md" file
> rg '^.*fast.*$' README.md -r FAST => Replaces the whole line from lines containing "fast" with "FAST" in "readme.md" file
```

### Filters

Ripgrep automatically ignores: (override all with flag `--no-ignore`)

* Files configured in `.gitignore` (override with flag `--no-require-git`)
* Hidden files (override with flag `--hidden` or `-.`)
* Binary files (override with flag `--text` or `-a`)
* Symbolic links (override with flag `--follow` or `-L`)

For convenience, can use `-u`, `-uu` and `-uuu` to override too.

* `-u`   : Ignores .gitignore filter
* `-uu`  : + Ignores hidden files and directories filter
* `-uuu` : + Ignores binary files filter 

If you have a `gitignore` file with:
```
log/
```

but you want `rg` to search for log files in that directory, you can
create the file `.ignore` (or `.rignore`) and disable it:
```
!log/
```

Precedence `.rignore` -> `.ignore` -> `.gitignore`

### List of some flags

See `rg --help` for a full list and full explanation.

* `--after-context=NUM`, `-A NUM`: Show NUM lines after
* `--before-context=NUM`, `-B NUM`: Show NUM lines before
* `--context=NUM`, `-C NUM`: Show NUM lines before and after
* `-u`, `-uu`, `-uuu`: Ignore filters `.gitignore`, hidden files, binary files
* `--files-with-matches`, `-l`: Show only filename, match found
* `--files-withot-match`: Show only filename, match not found
* `-c`: Show filename and number of lines with matches
* `--count-matches`: Show filename and number of matches (not num lines)
* `-I`: Do not print filename
* `--vimgrep`: For each match, print filename, line number and col number.
* `--sort=SORTBY`: Sort ascending (none, path, modified, ...)
* `--sortr=SORTBY`: Sort descending (none, path, modified, ...)
* `-quiet`, `-q`: Quiet - do not send to stdout. Ends at first match
* `--pretty`, `-p`: Pretty. Use colors, show heading and line number.
  Useful when pipping (`rg -p test | bat`), as per default pipping
  removes colors, headings and line numbers.
* `-o`: Print only the matched part of matching line.
* `--line-number`, `-n`: Show line number
* `--no-line-number`, `-N`: Do NOT show line number
* `--max-filesize=NUM+SUFFIX?`: Do not search in files with size > as X (50K, 80M, 1G ...) 
* `--max-depth=NUM`, `-D NUM`: Max recursive directories to search
* `--hidden`, `-.`: Search hidden files too
* `--follow`, `-L`: Search symlinks too
* `--binary`: Search in binary files too
* `--threads=NUM`, `-j NUM`: Aprox num threads to use
* `--text`, `-a`: Search binary files as if they where text
* `--case-sensitive`, `-s`: Case sensitive (default - override in config file)
* `--ignore-case`, `-i`: Case insensitive
* `--smart-case`, `-S`: Case insensitive if lowercase, case sensitive if at least one char uppercase.
* `--multiline`, `-U`: Lift restriction that a match can not include line terminator.
* `--search-zip`, `-z`: Search in gzip, bzip2, lzma, xz ... files. Decompression tools must be available in system.

## Configuration file

The path of the configuration file is set in variable `RIPGREP_CONFIG_PATH`.

There are two rules:

1. Every line is a shell argument, after trimming whitespace.
2. Lines starting with # (optionally preceded by any amount of whitespace) are ignored.

Example of a configuration file:

```
# Don't let ripgrep vomit really long lines to my terminal, and show a preview.
--max-columns=150
--max-columns-preview

# Add my 'web' type.
--type-add
web:*.{html,css,js}*

# Search hidden files / directories (e.g. dotfiles) by default
--hidden

# Using glob patterns to include/exclude files or folders
--glob=!.git/*

# or
--glob
!.git/*

# Set the colors.
--colors=line:none
--colors=line:style:bold

# Because who cares about case!?
--smart-case
```

Note: If you want to override a flag from the configuration file, you must give just 
give it in the command line:

`> rg --max-columns=0 test`

will override the value "150" from the configuration file example.

If you don't want that ripgrep reads the configuration file, use the `--no-config` flag.


## Advanced

Read the [Guide](https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md) for 
more advance features.
